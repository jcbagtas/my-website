#!/bin/bash
# AWS CLI v2
#
# Create an S3 Website
#
# Pre-requisite:
# 1. AWS CLI profile 
#       - brew install awscli
#       - Get an AWS Access Key ID and Secret Access Key from IAM > Users (This is old school method but ok for temporary, use SSO for security)
# 2. Route53 Hosted Zone
#       - buy a domain
#       - Point or register it in Route53
#       - Wait for propagation
#
# Syntax:
# createwebsite.sh <domain-name> <region> <hosted-zone-id> <profile>
# createwebsite.sh mysite.jcbagtas.com ap-southeast-1 Z2036MFRLL8N69 superjc


## S3 
echo "Initializing S3 Bucket"
# Create an S3 bucket 
aws s3 mb s3://$1 --region $2 --profile $4
# Enable S3 Website and set the default index and error documents.
aws s3 website s3://$1/ --index-document index.html --error-document error.html --profile $4
# Upload the index and error html fils from the www folder
aws s3 sync www s3://$1 --profile $4
# Make the bucket publicly readable
# After this, the main website endpoint will be accessible at http://[domain-name].s3-website-[region]].amazonaws.com/
aws s3api put-bucket-policy --bucket $1 --policy "{\"Statement\": [{\"Sid\": \"AddPerm\",\"Effect\": \"Allow\",\"Principal\": \"*\",\"Action\": [\"s3:GetObject\"],\"Resource\": [\"arn:aws:s3:::$1/*\"]}]}" --region $2 --profile $4
echo "Visit http://$1.s3-website-$2.amazonaws.com/"







###############################################################################
##                                                                           ##
## UNCOMMENT THE SECTION BELOW IF YOU HAVE YOUR DOMAIN AND HOSTED ZONE READY.##
## IF YOU JUST WANT TO TEST YOUR WEBSITE, KEEP THIS SECTION COMMENTED OUT.   ##
##                                                                           ##
###############################################################################


# ## ACM
# echo "Requesting SSL Certificate"
# # Request a certificate for your domain name
# CERT=$(aws acm request-certificate --region us-east-1 --domain-name $1 --validation-method DNS --profile $4 --query "CertificateArn" --output text)
# echo "please wait..."
# sleep 10
# echo "Validating SSL Certificate"
# # Create the DNS validation entry in the Hosted Zone
# # The validation may take a while
# RECORDNAME=$(aws acm describe-certificate --certificate-arn $CERT --profile $4 --region us-east-1 --query "Certificate.DomainValidationOptions[0].ResourceRecord.Name" --output text)
# RECORDVALUE=$(aws acm describe-certificate --certificate-arn $CERT --profile $4 --region us-east-1 --query "Certificate.DomainValidationOptions[0].ResourceRecord.Value" --output text)
# echo "Validate $RECORDNAME with $RECORDVALUE"
# R53CHANGEID2=$(aws route53 change-resource-record-sets --hosted-zone-id $3 --change-batch "{\"Changes\":[{\"Action\":\"CREATE\",\"ResourceRecordSet\":{\"Name\":\"$RECORDNAME\",\"Type\":\"CNAME\",\"TTL\":300,\"ResourceRecords\":[{\"Value\":\"$RECORDVALUE\"}]}}]}" --region us-east-1 --profile $4 --query "ChangeInfo.Id" --output text)
# # Sometimes this part takes up to 30mins
# echo "please wait... (10 -30mins... lol)"
# sleep 600


# ## CLOUDFRONT
# echo "Creating CloudFront Distribution"
# ## Setup the distribution configuration file
# sed -i "s/website-name/$1/g" distribution.json
# sed -i "s/website-region/$2/g" distribution.json
# sed -i "s|cert-arn|$CERT|g" distribution.json
# # Create a CloudFront distribution that points to your S3 Website Endpoint
# DISTRO=$(aws cloudfront create-distribution --distribution-config file://distribution.json --region us-east-1 --profile $4 --query "Distribution.DomainName" --output text)
# echo "please wait..."
# sleep 60
# # Revert the configuration file for future use
# sed -i "s/$1/website-name/g" distribution.json
# sed -i "s/$2/website-region/g" distribution.json
# sed -i "s|$CERT|cert-arn|g" distribution.json


# ## ROUTE53
# echo "Adding $1 Record to Hosted Zone $3"
# # Create a CNAME record in your Hosted Zone and point it to your CloudFron Distribution
# R53CHANGEID2=$(aws route53 change-resource-record-sets --hosted-zone-id $3 --change-batch "{\"Changes\":[{\"Action\":\"CREATE\",\"ResourceRecordSet\":{\"Name\":\"$1.\",\"Type\":\"CNAME\",\"TTL\":300,\"ResourceRecords\":[{\"Value\":\"$DISTRO.\"}]}}]}" --region us-east-1 --profile $4 --query "ChangeInfo.Id" --output text)


# Output
echo "S3 Bucket $1"
echo "ACM us-east-1 $CERT"
echo "Route53 CNAME $1 $DISTRO"
echo "CloudFront Distribution $DISTRO"
echo "Route53 CNAME $RECORDNAME $RECORDVALUE"
echo "DNS Validation Change Request $R53CHANGEID1"
echo "Route53 CHANGE ID 2 $R53CHANGEID2"
# My Website

A simple shell file to create an HTTPS ready website for an existing Domain name

## Pre-requisite:

1. AWS CLI profile 

        - Install awscli
        - Get an AWS Access Key ID and Secret Access Key from 
            - IAM > Users 
            - This is an old method but ok for temporary, use SSO for security

1. Route53 Hosted Zone

        - Buy a domain
        - Point or register it in Route53
        - Wait for propagation

## Usage

```shell
./createwebsite.sh sub.domain.com ap-southeast-1 Z2036MFRLL8N69 profile1
```
